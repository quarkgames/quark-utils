#!/usr/bin/env bash

usage() {
    echo -e "Usage:"
    echo -e "\t$0 -o <output file> -d <input directory>"
    exit 1
}

while getopts "ho:d:" opt; do
    case $opt in
        h)
            usage
            ;;
        o)
            OUTPUT_FILE=$OPTARG
            ;;
        d)
            INPUT_DIR=$OPTARG
            ;;
    esac
done

[[ ! $OUTPUT_FILE ]] && usage
[[ ! $INPUT_DIR ]] && usage

# Concatenate all files together, eliminating first '[' and last ']' in each
# Files are joined with a ','
REGEX="^\s*\[(.*)\]\s*$"
DELIMETER=""
FILES=$(find $INPUT_DIR -name "*.json" | sort -d -f)
echo "[" > $OUTPUT_FILE
for f in $FILES
do
    DATA=$(cat $f | tr -dc "[:alnum:][:space:][:punct:]")
    [[ $DATA =~ $REGEX ]]
    DATA=${BASH_REMATCH[1]}
    if [[ ${#DATA} = 0 ]]; then
        echo "skipping empty file $f"
    else
        echo "$DELIMETER" >> $OUTPUT_FILE
        echo "$DATA" >> $OUTPUT_FILE
        DELIMETER=","
    fi
done
echo "]" >> $OUTPUT_FILE
