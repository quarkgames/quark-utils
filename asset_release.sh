#!/usr/bin/env bash

GIT_UTILS_URL=https://bitbucket.org/quarkgames/git-utils/raw/master/
FETCH_SCRIPT_NAME=fetch_remote_tree.sh
SEMVER_SCRIPT_NAME=git-semver.sh

FETCH_SCRIPT_URL=$GIT_UTILS_URL$FETCH_SCRIPT_NAME
SEMVER_SCRIPT_URL=$GIT_UTILS_URL$SEMVER_SCRIPT_NAME

usage() {
    echo -e "Usage:"
    echo -e "\t$0 -o <output dir> -p <stats git path> -s <stats git url> -t <stats tag> -f"
    echo -e "\tone must supply either -p OR (-s and -t)"
    exit 1
}

ZIP=true
while getopts "hfo:s:t:p:" opt; do
    case $opt in
        f)
            ZIP=''
            ;;
        h)
            usage
            ;;
        o)
            OUTPUT_DIR=$OPTARG
            ;;
        p)
            STATS_PATH=$OPTARG
            ;;
        s)
            STATS_URL=$OPTARG
            ;;
        t)
            STATS_REF=$OPTARG
            ;;
    esac
done

if [[ $STATS_PATH ]]; then
    SOURCE=local
    [[ ! -e $STATS_PATH ]] && echo ERROR: path dne $STATS_PATH %% exit 1
    pushd $STATS_PATH;
    STATS_REF=$(git describe HEAD --tags --always)
    popd
elif [[ $STATS_URL && $STATS_REF ]]; then
    SOURCE=remote
else
    usage
fi
[[ ! $OUTPUT_DIR ]] && usage

TEMP_DIR=`mktemp -d XXXXXXX`
BIN_PATH=`pwd`/$TEMP_DIR/bin

echo "Get utility scripts"
mkdir -p $BIN_PATH
pushd $BIN_PATH
curl -O $FETCH_SCRIPT_URL
chmod +x $FETCH_SCRIPT_NAME
curl -O $SEMVER_SCRIPT_URL
chmod +x $SEMVER_SCRIPT_NAME
popd
FETCH_SCRIPT=$BIN_PATH/$FETCH_SCRIPT_NAME
SEMVER_SCRIPT=$BIN_PATH/$SEMVER_SCRIPT_NAME

if [[ $SOURCE == "remote" ]]; then
  echo "Fetch stats repository"
  STATS_PATH=$TEMP_DIR/stats
  $FETCH_SCRIPT -o $STATS_PATH -u $STATS_URL -r $STATS_REF
fi

STAGING_DIR=$TEMP_DIR/assembled
mkdir -p $STAGING_DIR

echo "Concatenate all stats into $TEMP_DIR/stats.json"
`dirname $0`/concat_json.sh -o $STAGING_DIR/stats.json -d $STATS_PATH

echo "Generate MANIFEST"
pushd $STATS_PATH;
VERSION=$($SEMVER_SCRIPT)
# derive the name from the remote url
NAME=$(basename $(git remote show -n origin | grep Fetch | cut -d: -f2- | tr '-' '_'))
NAME=${NAME%%.git}
popd
echo "$NAME, $VERSION" > $STAGING_DIR/MANIFEST

mkdir -p $OUTPUT_DIR
echo zip is $ZIP
if [[ $ZIP ]]; then
    echo "Creating tarball"
    OUTFILE=$STATS_REF.tar.gz
    tar -czf $OUTPUT_DIR/$OUTFILE -C $STAGING_DIR .
else
    cp -r $STAGING_DIR/* $OUTPUT_DIR
fi

echo "Clean up temp directory"
rm -fr $TEMP_DIR
